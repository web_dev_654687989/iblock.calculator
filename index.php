<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Расчет и уплата сборов");
?>
<div>
<?$APPLICATION->IncludeComponent(
	"namespace:iblock.calculator",
	".default",
	Array(
		"AJAX_MODE" => "N",
		"AJAX_OPTION_ADDITIONAL" => "",
		"AJAX_OPTION_HISTORY" => "N",
		"AJAX_OPTION_JUMP" => "N",
		"AJAX_OPTION_STYLE" => "N",
		"CACHE_TIME" => "86400",
		"CACHE_TYPE" => "A",
		"IBLOCK_ID" => "472",
		"IBLOCK_TYPE" => "calculators",
		"PROPERTY_FIELD_TYPE" => "type",
		"PROPERTY_FIELD_VALUES" => "values",
		"SECTIONS_TO_CATEGORIES" => "N",
		"SECTION_ID" => "",
		"SET_PAGE_TITLE" => "Y",
		"SET_SHOW_USER_FIELD" => "",
		"SHOW_RESULT" => "Y",
		"COMPONENT_TEMPLATE" => ".default"
	)
);?>
</div>
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>