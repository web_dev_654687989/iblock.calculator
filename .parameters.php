<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

if(!CModule::IncludeModule("iblock"))
	return;

// получаем список всех Типов инфоблоков
$arTypesEx = Array("-"=>" ");
$dbIblockType = CIBlockType::GetList(Array("SORT"=>"ASC"));
while($arRes = $dbIblockType->Fetch())
	if($arIBType = CIBlockType::GetByIDLang($arRes["ID"], LANG))
		$arTypesEx[$arRes["ID"]] = $arIBType["NAME"];

// получаем список всех инфоблоков выбранного Типа инфоблоков

$arIBlocks=Array();

$dbIblock = CIBlock::GetList(
	Array("SORT"=>"ASC"),
	Array(
		"SITE_ID"=>$_REQUEST["src_site"], 
		"TYPE" => ($arCurrentValues["IBLOCK_TYPE"]!="-"?$arCurrentValues["IBLOCK_TYPE"]:"")
	)
);
while($arRes = $dbIblock->Fetch())
	$arIBlocks[$arRes["ID"]] = "[" . $arRes["ID"] . "] " .   $arRes["NAME"];

//получаем список свойств выбранного раздела
$rsProp = CIBlockProperty::GetList(Array("sort"=>"asc", "name"=>"asc"), Array("ACTIVE"=>"Y", "IBLOCK_ID"=>(isset($arCurrentValues["IBLOCK_ID"])?$arCurrentValues["IBLOCK_ID"]:$arCurrentValues["ID"])));
while ($arr=$rsProp->Fetch())
{
	$arProperty[$arr["CODE"]] = "[".$arr["CODE"]."] ".$arr["NAME"];
	if (in_array($arr["PROPERTY_TYPE"], array("L", "N", "S")))
	{
		if ($arr['MULTIPLE']=='Y')
			$arProperty_M[$arr["CODE"]] = "[".$arr["CODE"]."] ".$arr["NAME"];
		else
			$arProperty_S[$arr["CODE"]] = "[".$arr["CODE"]."] ".$arr["NAME"];
	}
}

// массив входящих параметров
$arComponentParameters = array(
	"GROUPS" => array(
	),
	"PARAMETERS" => array(
		"IBLOCK_TYPE" => Array(
			"PARENT" => "BASE",
			"NAME" => GetMessage("WZ_TYPE"),
			"TYPE" => "LIST",
			"VALUES" => $arTypesEx,
			"DEFAULT" => "calculators",
			"REFRESH" => "Y"
		),
		"IBLOCK_ID" => Array(
			"PARENT" => "BASE",
			"NAME" => GetMessage("WZ_IBLOCK"),
			"TYPE" => "LIST",
			"VALUES" => $arIBlocks,
			"DEFAULT" => $arCurrentValues["IBLOCK_ID"],
			"ADDITIONAL_VALUES" => "N",
			"REFRESH" => "Y"
		),
		"PROPERTY_FIELD_TYPE" => Array(
			"PARENT" => "BASE",
			"NAME" => GetMessage("WZ_PROPERTY"),
			"TYPE" => "LIST",
			"VALUES" => $arProperty_S
		),
		"PROPERTY_FIELD_VALUES" => Array(
			"PARENT" => "BASE",
			"NAME" => GetMessage("WZ_PROPERTY_VALUES"),
			"TYPE" => "LIST",
			"VALUES" => $arProperty_M
		),
		"CACHE_TIME"  =>  Array("DEFAULT"=>86400),
		"AJAX_MODE" => array(),
	),
);

?>
