let add_s = document.getElementById('summa_0').innerHTML; // добавляемая строка
let add_b = document.getElementById('trebovania_0').innerHTML; // добавляемый блок

var krestik = document.querySelector('.del_b_button'); //находим единственный блок и скрываем у него крестик
var krestik_id = krestik.getAttribute('id');
document.getElementById(krestik_id).style.visibility = 'hidden';

var counter_m = 1; //id строки и блока удаления
var m = 1;
var n = 1; //id строки и кнопки удаления ----ПОЧЕМУ 3 ---проверить не нужно ли поменять обратно на 3
var counter_b = 1;
var colvo_strok; //для подсчета строк в каждом блоке

var use_USD = false;
var use_EUR = false;

var inp = document.querySelector('.all_blocks');
inp.addEventListener('input', function(event){
    count();
})
inp.addEventListener('click', function(event){
    count();
})
inp.addEventListener('change', function(event){
    count();
})

function add_summa(block_where){
    //добавление строки

    var where_is_this_string;
    if (block_where.length == 11)
        where_is_this_string = block_where.substring(block_where.length - 1);  //конец id блока
    else if (block_where.length == 12)
        where_is_this_string = block_where.substring(block_where.length - 2);  //конец id блока
    else 
        where_is_this_string = block_where.substring(block_where.length - 3);  //конец id блока
    

   var vse_trebovania = document.getElementsByClassName('trebovania'); //массив блоков, где нужно найти тот, который с id как окончание block_where
   var vse_trebovania_length = vse_trebovania.length;

    for (var x = 0; x < vse_trebovania_length; x++){

        var sravnenie_1;
        var sravnenie_2 = where_is_this_string;

        var my_block = vse_trebovania[x].id;


        if (my_block.length == 12)
            sravnenie_1 = my_block.substring(my_block.length - 1);  //конец id блока
        else if (my_block.length == 13)
            sravnenie_1 = my_block.substring(my_block.length - 2);  //конец id блока
        else 
            sravnenie_1 = my_block.substring(my_block.length - 3);  //конец id блока



        if (sravnenie_1 === sravnenie_2){

            var add = document.createElement('div'); //определение добавляемого блока
            var add_id = 'summa_' + n; //id добавляемого блока
          
            add.id = add_id; // присваеваем id к добавленному блоку
            add.className = 'summa';
            add.innerHTML = add_s; // определение внутренности добавляемого блока
            var content = document.querySelector('#trebovania_' + sravnenie_1); // находим блок, куда будем добавлять новый (add)
            var content_1 = content.querySelector('#for_summas');
            
            content_1.appendChild(add);
        
            colvo_strok = content.getElementsByClassName('del_button');
            if (colvo_strok.length > 1){
                for(var i = 0; i < colvo_strok.length; i++){
                    colvo_strok[i].classList.add('del_button_visible'); //чтобы сделать видимыми
                    colvo_strok[i].classList.add('trebovania_' + sravnenie_1); //чтобы сделать невидимыми
                }
            }

            var str_but_id = document.querySelector('#summa_' + n).querySelector('.del_button');
            str_but_id.id = n;

            n++;
        } 
        
    }

 
}

function add_trebovania(){
    //добавление формы
//НАЙТИ КНОПКУ ПО КЛАССУ И ЗАДАТЬ ЕЙ ID ЗДЕСЬ  

    var add_block = document.createElement('div'); //определение добавляемого блока
    var add_block_id = 'trebovania_' + m; //id маленького (внутреннего) добавляемого блока
    add_block.id = add_block_id;

    add_block.classList.add('trebovania');
    add_block.innerHTML = add_b; // определение внутренности добавляемого блока
    
    add_block.querySelector('#summa_0').id = 'summa_' + n; //меняем id первой строки добавляемого блока
    
    

    var content = document.querySelector('#isk'); // находим блок, куда будем добавлять новый (add_block)
    content.appendChild(add_block);
    //content.innerHTML = content.innerHTML + "Арбитражное соглашение (контракт) №" + counter_m;
  

    var block_del_button_id = document.querySelectorAll('.del_b_button');  // нахождение id всех кнопок
    block_del_button_id[counter_m].id = m; //присвоение id кнопки в появляющемся блоке

    var block_add_button_id = document.querySelectorAll('.add_summa_button'); //кнопка для добавления новой строчки в блоке
    block_add_button_id[counter_m].id = "add_summa_" + m;


    var add_contract = document.querySelectorAll('.contract'); 
    add_contract[counter_m].id = "contract_" + (counter_m + 1); 
    var in_contract = add_contract[counter_m].innerHTML;

    in_contract_count = in_contract.substring(0, in_contract.length - 1);
    add_contract[counter_m].innerHTML = in_contract_count + (counter_m + 1);

    var str_but_id = document.querySelector('#summa_' + n).querySelector('.del_button');
    str_but_id.removeAttribute('id');
    str_but_id.setAttribute('id', n);

    n++;
    m++;
    counter_m++;
    counter_b++;


    if (counter_b > 1){
        var elem = document.querySelector('.del_b_button');
        var elemId = elem.getAttribute('id');
       document.getElementById(elemId).style.visibility = 'visible';
    } 
}
 

function del_summa(n, cl, block_where){ //  n - id строки, m - id блока

        //удаление строки
        var min_id = n; //определение id строки для удаления
        var del_id = 'summa_' + min_id;
        //var del_from = document.getElementById(block_id);  //Нахождение блока со строкой по id
        var del_str = document.getElementById(del_id);  //Нахождение строки по id

//Находим кнопку, на которую нажали
        var del_but = del_str.querySelector('.del_button');
//Находим id нажатой кнопки
        var del_but_id = del_but.id;

//Находим требование
        var del_but_treb = del_but.parentNode.parentNode.parentNode.parentNode; //trebovania
//Находим id ребования
        var del_but_treb_id = del_but_treb.id; //trebovania id

//Находим кол-во строк, оставшихся в блоке
        colvo_strok = del_but_treb.getElementsByClassName('del_button').length;
        //alert(colvo_strok);

        if (colvo_strok < 3){
            del_str.remove();
            var cl = del_but_treb.getElementsByClassName('del_button')[0].classList.remove('del_button_visible');
        } else del_str.remove();

}


function del_trebovania(m){
    var l = document.getElementsByClassName('trebovania').length;
            var block_id = m; //определение id блока, где строка для удаления ---------------------------------------------------------------

            var del_id = 'trebovania_' + block_id; // полный id блока
            var del_b = document.getElementById(del_id);  //Нахождение блока по id
            
            del_b.remove(); // удаление блока
            counter_m--; 
            counter_b--;

            if (counter_b == 1){ //убираем кнопку для удаления блока, если остался 1 блок
                //alert("УБРАТЬ КРЕСТ");
                var elem = document.querySelector('.del_b_button');
                var elemId = elem.getAttribute('id');
               document.getElementById(elemId).style.visibility = 'hidden';
            }

            var add_contract = [];
            var add_contract = document.querySelectorAll('.contract'); 
            for(var i = 0; i<(counter_b+1); i++){
                var add_const_id = add_contract[i];
                add_const_id.innerHTML = "Арбитражное соглашение (контракт) №" + String((i+1));
            }
}






function count(){ //По кнопке "Рассчитать"

    use_EUR = false;
    use_USD = false;
    var razdelitel = '<br><div class="razdelitel"></div><br>';
    var summa_trebovanii;
    var cena_iska;
    var arbitr_sbor;
    var registr_sbor;
    var obespech_sbor = '';
    var itog_sborov;
    var summa_sborov;

var chislo; //берется введенное число (требование)
var valuta; //берется введенная валюта
const spor = document.querySelector('input[type="radio"][name="type"]:checked').value; //Вид арбитража - для общего рассчета берется только один
var sostav = document.querySelector('input[name="sostav_suda"]:checked').value; //Состав тритейского суда - для общего рассчета выбирается один вариант


var isk_cost; //Цена иска / сумма требований
var reg_sbor;
var arb_sbor;
var ob_sbor = 0;
var summa_iska = 0;


var course_RUB_USD = document.getElementById('USD').value; //
var course_RUB_EUR = document.getElementById('EUR').value; //


var perevod; //для записи валюты в долларах (доллар по умолчанию)
var c; //счетчик для while

var zapolnenie = true;


var trebovania_by_Class = document.getElementsByClassName('trebovania');
var trebovania_kol_vo = trebovania_by_Class.length;
var trebovania_by_Id = [];
var vse_arb_sbori = [];
var isk_v_kontrakte = [];
var edinizi_v_kontrakte = [];
var ediniza = '';
var edinizi = '';
var apell_sbor = 0;
var apellaz_sbor = '';

if (spor == 'sport_arb'){
    document.getElementById('apell_block').style.display = 'flex';
    console.log(spor);
} else {
    document.getElementById('apell_block').style.display = 'none';
    document.querySelector('input[id="apell"]').checked = false;
    console.log(spor);
}

    if ((spor == 'corp_arb') || (spor == 'comm_arb')){ //всё должно быть в долларах


            for(var i = 0; i<trebovania_kol_vo; i++){ //цикл для того, чтобы считать арб сбор ДЛЯ КАЖДОГО ТРЕБОВАНИЯ (БЛОКА)
                arb_sbor = 0;
                isk_cost = 0;

                trebovania_by_Id[i] = trebovania_by_Class[i].id; //нашлись id блоков, которые сейчас есть, они записаны в массив
            
                chislo = trebovania_by_Class[i].getElementsByClassName('form_chislo'); //значение в каждой строке
                valuta = trebovania_by_Class[i].getElementsByClassName('form_valuta'); //валюта для каждой строки 

                c = 0;
                while (c<chislo.length){ // Расчет цены иска в долларах

                    if(chislo[c].value == ''){
                        var boxes = document.getElementsByClassName('chislo_box');
                        for(var boxes_count = 0; boxes.length > boxes_count; boxes_count++){
                            boxes[boxes_count].classList.add('chislo_box_proverka');
                        }

                        zapolnenie = false;
                        break;
                    }
                    
                    if (c == 0){
                        if(valuta[c].value == "EUR"){
                            perevod = Number(chislo[c].value) * course_RUB_EUR / course_RUB_USD;
                            ediniza = Intl.NumberFormat("ru-RU").format(chislo[c].value) + ' * ' + course_RUB_EUR + ' / ' + course_RUB_USD + '<br>';
                            use_USD = true;
                            use_EUR = true;
                        } else if(valuta[c].value == "RUB"){
                            perevod = Number(chislo[c].value) / course_RUB_USD;
                            ediniza = Intl.NumberFormat("ru-RU").format(chislo[c].value) + ' / ' + course_RUB_USD + '<br>';
                            use_USD = true;
                        } else {
                            perevod = Number(chislo[c].value);
                            ediniza = Intl.NumberFormat("ru-RU").format(chislo[c].value) + '<br>';            
                        }
                    } else {
                        if(valuta[c].value == "EUR"){
                            perevod = Number(chislo[c].value) * course_RUB_EUR / course_RUB_USD;
                            ediniza = ' + ' + Intl.NumberFormat("ru-RU").format(chislo[c].value) + ' * ' + course_RUB_EUR + ' / ' + course_RUB_USD + '<br>';
                            use_USD = true;
                            use_EUR = true;
                        } else if(valuta[c].value == "RUB"){
                            perevod = Number(chislo[c].value) / course_RUB_USD;
                            ediniza = ' + ' + Intl.NumberFormat("ru-RU").format(chislo[c].value) + ' / ' + course_RUB_USD + '<br>';
                            use_USD = true;
                        } else {
                            perevod = Number(chislo[c].value);
                            ediniza = ' + ' + Intl.NumberFormat("ru-RU").format(chislo[c].value) + '<br>';            
                        }
                    }
                
                    edinizi = edinizi + ediniza;

                    isk_cost = isk_cost + perevod;

                    c++;
                } 
                isk_v_kontrakte[i] = isk_cost;
                summa_iska = summa_iska + isk_cost;

                edinizi_v_kontrakte[i] = edinizi;
                edinizi = '';


                reg_sbor = 1000; //USD


                if (isk_cost<10000) {
                    arb_sbor = 3000;
                }
                else if (10000<=isk_cost && isk_cost<50000){
                    arb_sbor = 3000 + (isk_cost-10000) * 0.125;
                }
                else if (50000<=isk_cost && isk_cost<100000){
                    arb_sbor = 8000 + (isk_cost-50000) * 0.11;
                }
                else if (100000<=isk_cost && isk_cost<200000){
                    arb_sbor = 13500 + (isk_cost-100000) * 0.06;
                }
                else if (200000<=isk_cost && isk_cost<500000){                    
                    arb_sbor = 19500 + (isk_cost-200000) * 0.03;
                }
                else if (500000<=isk_cost && isk_cost<1000000){                    
                    arb_sbor = 28500 + (isk_cost-500000) * 0.018;
                }
                else if (1000000<=isk_cost && isk_cost<2000000){                    
                    arb_sbor = 37500 + (isk_cost-1000000) * 0.01;
                }
                else if (2000000<=isk_cost && isk_cost<5000000){  //здесь нужно передвинуть границу                
                    arb_sbor = 47500 + (isk_cost-2000000) * 0.006;
                }
                else if (5000000<=isk_cost && isk_cost<10000000){                    
                    arb_sbor = 65500 + (isk_cost-5000000) * 0.005;
                }
                else {                    
                    arb_sbor = 90500 + (isk_cost-10000000) * 0.0014;
                }


                vse_arb_sbori[i] = arb_sbor;
            }

            for (var i = 0; i<(vse_arb_sbori.length-1); i++){
                arb_sbor = arb_sbor + vse_arb_sbori[i];
            }

            if(sostav == 'edinolich'){
                arb_sbor = arb_sbor.toFixed(0) * 0.8;
            }

            if (obespech.checked == true) { 
                ob_sbor = 30000;
                obespech_sbor = 'Обеспечительный сбор + ' + Intl.NumberFormat("ru-RU").format(ob_sbor.toFixed(0)) + ' ₽<br>'; 
                itog_sborov = Intl.NumberFormat("ru-RU").format((arb_sbor + reg_sbor).toFixed(0)) + ' + ' + String(Intl.NumberFormat("ru-RU").format(ob_sbor)) + ' ₽';
            } else {
                itog_sborov = arb_sbor + reg_sbor + ob_sbor;
                itog_sborov = Intl.NumberFormat("ru-RU").format(itog_sborov.toFixed(0));
            } 

           

                var res;
                var ceni_iskov = '';

                    for (var i = 0; i < isk_v_kontrakte.length; i++)
                    {
                        if(i == 0){ //для первого арб согл
                            res = isk_v_kontrakte[0];
                            ceni_iskov = '<div class="blue"> Арбитражное соглашение (контракт) №' + (i+1) + '</div><br>' + edinizi_v_kontrakte[0] + '____________<br>= $ ' + Intl.NumberFormat("ru-RU").format(res.toFixed(2)) + '<br><br>';
                        } else{ //для всех остальных арб согл (не 1)
                            ceni_iskov = ceni_iskov +  '<div class="blue"> Арбитражное соглашение (контракт) №' + (i+1) + '</div><br>' + edinizi_v_kontrakte[i] + '____________<br>= $ ' + Intl.NumberFormat("ru-RU").format(isk_v_kontrakte[i].toFixed(2)) + '<br><br>';
                        }
                    }
                    ceni_iskov = ceni_iskov + '<br>';



                summa_iska = Intl.NumberFormat("ru-RU").format(summa_iska.toFixed(2));
                arb_sbor = Intl.NumberFormat("ru-RU").format(arb_sbor.toFixed(0));
                reg_sbor = Intl.NumberFormat("ru-RU").format(reg_sbor.toFixed(0));

                summa_trebovanii = "Сумма требований арбитражного соглашения (контракта) + $ " + summa_iska + '<br>';
                cena_iska = "<div class='main_result'>Цена иска = $ " + summa_iska + '</div>';
                arbitr_sbor = "Арбитражный сбор + $ " + arb_sbor + '<br>';
                registr_sbor = "Регистрационный сбор + $ " + reg_sbor + '<br>';
                summa_sborov = "<div class='main_result'>Сумма сборов = $ " + itog_sborov + "</div>";

    } else {                                          //всё должно быть в рублях

            for(var i = 0; i<trebovania_kol_vo; i++){ //цикл для того, чтобы считать арб сбор ДЛЯ КАЖДОГО ТРЕБОВАНИЯ
                arb_sbor = 0;
                isk_cost = 0;

                trebovania_by_Id[i] = trebovania_by_Class[i].id; //нашлись id блоков, которые сейчас есть, они записаны в массив
            
                chislo = trebovania_by_Class[i].getElementsByClassName('form_chislo'); //значение в каждой строке
                valuta = trebovania_by_Class[i].getElementsByClassName('form_valuta'); //валюта для каждой строки 

                c = 0;
                while (c<chislo.length){ // Расчет цены иска в рублях

                    if(chislo[c].value == ''){
                        var boxes = document.getElementsByClassName('chislo_box');
                        for(var boxes_count = 0; boxes.length > boxes_count; boxes_count++){
                            boxes[boxes_count].classList.add('chislo_box_proverka');
                        }

                        zapolnenie = false;
                        break;
                    }

                    if (c == 0){
                        if(valuta[c].value == "EUR"){
                            perevod = Number(chislo[c].value) * course_RUB_EUR;
                            ediniza = Intl.NumberFormat("ru-RU").format(chislo[c].value) + ' * ' + course_RUB_EUR + '<br>';
                            use_EUR = true;
                        } else if(valuta[c].value == "USD"){
                            perevod = Number(chislo[c].value) * course_RUB_USD;
                            ediniza = Intl.NumberFormat("ru-RU").format(chislo[c].value) + ' * ' + course_RUB_USD + '<br>';
                            use_USD = true;
                        } else {
                            perevod = Number(chislo[c].value);
                            ediniza = Intl.NumberFormat("ru-RU").format(chislo[c].value) + '<br>';
                        }
                    } else {
                        if(valuta[c].value == "EUR"){
                            perevod = Number(chislo[c].value) * course_RUB_EUR;
                            ediniza = ' + ' + Intl.NumberFormat("ru-RU").format(chislo[c].value) + ' * ' + course_RUB_EUR + '<br>';
                            use_EUR = true;
                        } else if(valuta[c].value == "USD"){
                            perevod = Number(chislo[c].value) * course_RUB_USD;
                            ediniza = ' + ' + Intl.NumberFormat("ru-RU").format(chislo[c].value) + ' * ' + course_RUB_USD + '<br>';
                            use_USD = true;
                        } else {
                            perevod = Number(chislo[c].value);
                            ediniza = ' + ' + Intl.NumberFormat("ru-RU").format(chislo[c].value) + '<br>';
                        }
                    }

                    edinizi = edinizi + ediniza;

                    isk_cost = isk_cost + perevod;

                    c++;
                } 
                isk_v_kontrakte[i] = isk_cost; //кол-во сумм в каждом (i) требовании
                summa_iska = summa_iska + isk_cost;

//целая строчка для одного арб согл

                edinizi_v_kontrakte[i] = edinizi;
                edinizi = '';

                reg_sbor = 10000; //RUB

                if (apell.checked == true) {
                    apell_sbor = 50000; 
                    document.querySelector('input[value="kollegia"]').checked = true;
                    arb_sbor = 0;
                    apellaz_sbor = 'Апелляционный сбор + ' + Intl.NumberFormat("ru-RU").format(apell_sbor) + ' ₽<br>';
                } 
                else if (isk_cost<100000) {
                    arb_sbor = 10000;
                }
                else if (100000<=isk_cost && isk_cost<200000){
                    arb_sbor = 10000 + (isk_cost-100000) * 0.02;
                }
                else if (200000<=isk_cost && isk_cost<1000000){
                    arb_sbor = 12000 + (isk_cost-200000) * 0.018;
                }
                else if (1000000<=isk_cost && isk_cost<2000000){
                    arb_sbor = 26400 + (isk_cost-1000000) * 0.008;
                }
                else if (2000000<=isk_cost && isk_cost<10000000){
                    arb_sbor = 34400 + (isk_cost-2000000) * 0.005;
                }
                else if (10000000<=isk_cost && isk_cost<30000000){
                    arb_sbor = 74400 + (isk_cost-10000000) * 0.004;
                }
                else if (30000000<=isk_cost && isk_cost<50000000){
                    arb_sbor = 154400 + (isk_cost-30000000) * 0.003;
                }
                else if (50000000<=isk_cost && isk_cost<70000000){
                    arb_sbor = 214400 + (isk_cost-50000000) * 0.002;
                }
                else {
                    arb_sbor = 254400 + (isk_cost-70000000) * 0.0015;
                    if (arb_sbor>900000){
                        arb_sbor = 900000;
                    }
                }


                vse_arb_sbori[i] = arb_sbor;
            }

            for (var i = 0; i<(vse_arb_sbori.length-1); i++){
                 arb_sbor = arb_sbor + vse_arb_sbori[i];
            }

            if(sostav == 'edinolich'){
                arb_sbor = arb_sbor.toFixed(0) * 0.8;
            }


            if (obespech.checked == true) { 
                ob_sbor = 30000;
                obespech_sbor = 'Обеспечительный сбор + ' + Intl.NumberFormat("ru-RU").format(ob_sbor) + ' ₽<br>'; 
            }

            itog_sborov = arb_sbor + reg_sbor + ob_sbor + apell_sbor;


            summa_iska = Intl.NumberFormat("ru-RU").format(summa_iska.toFixed(2));
            arb_sbor = Intl.NumberFormat("ru-RU").format(arb_sbor.toFixed(0));
            reg_sbor = Intl.NumberFormat("ru-RU").format(reg_sbor.toFixed(0));
            itog_sborov = Intl.NumberFormat("ru-RU").format(itog_sborov.toFixed(0));


            var res;
            var ceni_iskov = '';

            for (var i = 0; i < isk_v_kontrakte.length; i++)
            {
                    if(i == 0){ //для первого арб согл
                        res = isk_v_kontrakte[0];
                        ceni_iskov = '<div class="blue"> Арбитражное соглашение (контракт) №' + (i+1) + '</div><br>' + edinizi_v_kontrakte[0] + '____________<br>= ' + Intl.NumberFormat("ru-RU").format(res.toFixed(2)) + ' ₽<br><br>';
                    } else{ //для всех остальных арб согл (не 1)
                        ceni_iskov = ceni_iskov +  '<div class="blue"> Арбитражное соглашение (контракт) №' + (i+1) + '</div><br>' + edinizi_v_kontrakte[i] + '____________<br>= ' + Intl.NumberFormat("ru-RU").format(isk_v_kontrakte[i].toFixed(2)) + ' ₽<br><br>';
                    }
            }
            ceni_iskov = ceni_iskov + '<br>';
            
                cena_iska = "<div class='main_result'>Цена иска = " + summa_iska + ' ₽</div>';
                registr_sbor = "Регистрационный сбор + " + reg_sbor + ' ₽<br>';
                summa_sborov = "<div class='main_result'>Сумма сборов = " + itog_sborov + " ₽</div>";

                if (arb_sbor == 0) {
                    arbitr_sbor = '';
                } else arbitr_sbor = "Арбитражный сбор + " + arb_sbor + ' ₽<br>';
            
               
    }


    if (zapolnenie == true){ //вывод результатов
        
        document.getElementById('big_block_for_results').style.display = 'flex';

        document.getElementById('block_for_results').style.display = 'flex';
        var block_itog = document.getElementById('block_for_results');
        block_itog.innerHTML = "";
    
  
    //_________________________________________________________________________________________________________________________________________________________

        
       
        var rasch_course = '';
        if((use_EUR === true) && (use_USD === true)){
            rasch_course = 'Расчетный курс <br> € 1 = ' + course_RUB_EUR + ' ₽<br>' + 
            '$ 1 = ' + course_RUB_USD + ' ₽<br>' + razdelitel;
        }else if(use_USD === true){
            rasch_course = 'Расчетный курс <br> $ 1 = ' + course_RUB_USD + ' ₽<br>' + razdelitel;
        }else if(use_EUR === true){
            rasch_course = 'Расчетный курс <br> € 1 = ' + course_RUB_EUR + ' ₽<br>' + razdelitel;
        }else rasch_course = '';


        block_itog.innerHTML = block_itog.innerHTML +
            rasch_course + 
            ceni_iskov +  /*summa_trebovanii +*/ 
            cena_iska + razdelitel + 
            arbitr_sbor + apellaz_sbor + registr_sbor + obespech_sbor + '<br>' + 
            summa_sborov;

        }
}

function save_count(){
    var container = document.getElementById("block_for_results"); /* full page */
    html2canvas(container, { allowTaint: true }).then(function (canvas) {
        var link = document.createElement("a");
        document.body.appendChild(link);
        link.download = "result.jpg";
        link.href = canvas.toDataURL();
        link.click();
    });
}

function clear_count(){
    window.location.reload();
}