<?php if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>

<?php 
	// подключаем модуль currency
	\Bitrix\Main\Loader::includeModule('currency');
?>
<?php 
// Currencies
$rsCurrency = \Bitrix\Currency\CurrencyRateTable::getList([
  'filter'  =>  ['=CURRENCY' => 'USD'], 
  'order'   =>  ['DATE_RATE' => 'DESC']
]);

$Currencies = $rsCurrency->fetchAll();
$usd = $Currencies[0]['RATE'];

$rsCurrency = \Bitrix\Currency\CurrencyRateTable::getList([
  'filter'  =>  ['=CURRENCY' => 'EUR'], 
  'order'   =>  ['DATE_RATE' => 'DESC']
]);

$Currencies = $rsCurrency->fetchAll();
$eur = $Currencies[0]['RATE'];
?>

<?php
if ($arResult['ERROR'])
	echo '<div><font class=wizard_errortext>'.$arResult['ERROR'].'</font></div>';
elseif($arResult['MESSAGE'])
	echo '<div><font class=wizard_oktext>'.$arResult['MESSAGE'].'</font></div>';
?>

<div>
	<input type=hidden name=LAST_SECTION_ID value="<?=$arResult['LAST_SECTION_ID']?>">
	<input type=hidden name=CURRENT_STEP value="<?=$arResult['CURRENT_STEP']?>">
	<input type=hidden name=PATH value="<?=$this->getComponent()->getPath();?>">
</div>

<div class="all_blocks">
	<div class="big_block">
        <div class="block"> <!-- Блоки с исковыми требованиями -->
                <div class="block_part name"> <!-- Заголовок страницы -->
					<?=GetMessage("WZ_TITLE")?>
                </div>

<?php
	if (count($arResult['SECTIONS']))
	{
		foreach($arResult['SECTIONS'] as $f)
		{
			$id = $f['ID'];					
		}
	}

	if (count($arResult['FIELDS']))
	{
		$i=0;
		foreach($arResult['FIELDS'] as $num=>$f)
		{
			$id = $f['FIELD_ID'];

			// перечисляем все типы полей
			if ($f['FIELD_TYPE']=='contract') // contract fild`s type options
			{
				echo '<div id="isk">' . 
					'<div id="trebovania_0" class="trebovania"><!-- Блок с требованиями -->' .
						'<div class="block_part name_1"> <!-- Заголовок требования и скрытая кнопка для его удаления -->' . 
							'<div id="'. $f['FIELD_TYPE'] . '" class="'. $f['FIELD_TYPE'] . '">' . $f['NAME'] . '</div>' .
							'<button class="del_b_button" id="0" onclick="del_trebovania(id)" title="Удалить контракт"> ⨯ </button>' . 
						'</div>';
			}
			elseif ($f['FIELD_TYPE']=='number') // number field (сумма требований)
			{
				echo 	'<div id="for_summas"> <!-- инпуты -->' .
							'<div id="summa_0" class="summa">' . 
								'<div>' .															
									'<div class="form chislo_box chislo_box_proverka">' .
										'<input id="' . $f['FIELD_TYPE'] . '" type="' . $f['FIELD_TYPE'] . '" min="0" class="form form_chislo" step="0.0001" required="required" placeholder="' . $f['NAME'] . '"/>' .
							'<span>' . $f['NAME'] . '</span>' .
						'</div>';
			}
			elseif ($f['FIELD_TYPE']=='currency_types') // select box for currencies list
			{
				echo '<div class="form form_1" id="values">' .
							'<select name="value" id="value" class="form_valuta">'; 
								foreach($f['FIELD_VALUES'] as $v){
									echo '<option id="value_' . $v . '" value="' . $v . '">';
									switch ($v) {
										case "USD": 
											echo "$ - USD (Доллар)"; break;
										case "RUB":
											echo "₽ - RUB (Рубль)"; break;
										case "EUR":
											echo "€ - EUR (Евро)"; break;
										default: 
											echo "Валюта не найдена!"; break;
									};
									echo '</option>';
								} // end foreach
							echo '</select>' . 
						'</div>' . 
						'<button id="0" onclick="del_summa(id)" class="button button_1 del_button" title="Удалить сумму искового требования"> ⨯ </button>' . 
					'</div>' .
				'</div>' .
			'</div>';
			}
			elseif ($f['FIELD_TYPE']=='plus_button') // plus button
			{
				echo '<div class="block_part"> <!-- Кнопка + -->' . 
							'<button onclick="add_summa(id)" id="add_summa_0" class="button button_1 underline' . ' add_summa_button" title="' . $f['PREVIEW_TEXT'] . '"> ' . $f['NAME'] . ' </button>' . 
						'</div>' . 
					'</div>' .                     
				'</div>' . 
			'</div>';
			}
			elseif ($f['FIELD_TYPE']=='add_button') // add button
			{
				echo '<div class="block"> <!-- Кнопка добавить требования -->' . 
						'<button onclick="add_trebovania()" class="button button_2 underline">' . $f['NAME'] . '</button>' . 
					'</div>' . 
					'<div class="block little_block">' . 
						'<div class="block">' . 
							'<div class="block_part paranetrs">';
			}
			elseif ($f['FIELD_TYPE']=='checkbox') // checkbox
			{
				foreach($f['FIELD_VALUES'] as $v)
					echo '<input id="'.$v.'" class="check" type="checkbox"/>' . $f['NAME'] . '<br>';
			}
			elseif ($f['FIELD_TYPE']=='exchange_rates') // exchange rates
			{
				echo '</div>' .
				'</div>' .
			'</div>' .
				'<div class="name">' .$f['NAME'] . '</div>' . 
					'<div class="block block_valut"> <!-- Валюты -->';
				foreach($f['FIELD_VALUES'] as $v){
					echo '<div class="form chislo_box">' . 
								'<input id="';
					switch ($v) {
					case "USD": 
						echo $v . '" type="number" placeholder="Курс $ (' . $v . ') в рублях" min="0" step="0.0001" class="form form_1"
								required="required" value="' . $usd . '" />' . 
							'<span>Курс доллара США $ (' . $v . ') в рублях</span>' . 
						'</div>'; break;
					case "EUR":
						echo $v . '" type="number" placeholder="Курс € (' . $v . ') в рублях" min="0" step="0.0001" class="form form_1"
								required="required" value="' . $eur . '" />' . 
							'<span>Курс евро € (' . $v . ') в рублях</span>' . 							
						'</div>'; break;
					default: 
						echo "Курс валюты не найден!"; break;
					};
				}
			}
			elseif ($f['FIELD_TYPE']=='date') // date of exchange rates
			{							
				echo '<div class="block block_exchange_date">' . 
						'<!-- Дата курсов валют -->' . 
						'<div class="form exchange_date">' .
						'<input type="text" id="exchange_date" class="form form_1" placeholder="' . $f['NAME'] . '" data-date-format="d.m.Y" />' .
					'<span>' . $f['NAME'] . '</span>' .
						'</div>' .			
					'</div>' . 
					'<p id="ajax_error"></p>' .
				'</div>';
			}
			unset($arResult['FIELDS'][$field_num]['FIELD_VALUE']);
		}
	}

?>

	</div>
	
	<div id="big_block_for_results" class="big_block big_block_for_results">
		<button onclick="clear_count()" class="button button_2 clear_result"><div class="clear_button_text">Очистить результат</div><div class="clear_button">⨯</div></button>
		<div id="block_for_results" class="block block_for_results"></div>
		<button onclick="save_count()" class="button button_2 save_result"><div class="save_result_button">Скачать результат </div><div class="save_result_button save_strelka"> 🡇 </div></button> <!--  🡇🡅  -->
	</div>
	
	<div id="hidden" style="display: none;"></div>
</div>

<?php
include_once 'script.php';
?>