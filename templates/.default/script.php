<script>
	const componentPath = document.querySelector('[name=PATH]').value; // путь до папки компонента
	const inputUsd = document.querySelector('#USD'); // ищем поле ввода курса USD
	const inputEur = document.querySelector('#EUR'); // ищем поле ввода курса EUR
	inputUsd.value = "<?php echo $usd; ?>"; // выводим курс USD на текущую дату
	inputEur.value = "<?php echo $eur; ?>"; // выводим курс EUR на текущую дату

	flatpickr.localize(flatpickr.l10ns.ru);	// подключаем ru локализацию календаря

	const resArr = []; // объявляем пустой массив для записи дат

	var xhr = new XMLHttpRequest(); // через ajax-запрос вытаскиваем список доступных дат
	xhr.open('GET', componentPath + "/get_list_date.php", false); // отправляем на сервер GET-запрос без параметров
	xhr.send();

	if (xhr.status != 200) {
		// обработать ошибку, если статус запроса вернул не 200
		if (xhr.status == 404) {
			// обработка ошибки
			document.getElementById("ajax_error").innerHTML = ( 'Ошибка ' + xhr.status + ': файл запроса курсов валют не существует' );
		} else {
			document.getElementById("ajax_error").innerHTML = ( 'Ошибка ' + xhr.status + ': что-то пошло не так' );
		}
	} else {
		// если статус запроса вернул 200, вывести результат в текстовом формате
		const resultText = xhr.responseText.replace(/\n/g, '').trim().slice(0, -1); // responseText -- текст ответа, убираем лишние символы
		window.resArr = resultText.split(', '); // используем метод split с указанием разделителя для преобразования строки в массив
	}

	flatpickr("#exchange_date", { // объявляем календарь
		enableTime: false, // отключаем выбор времени в календаре
		dateFormat: "d.m.Y", // формат даты, используемый в календаре
		defaultDate: window.resArr[0], // выводим в календаре дату по-умолчанию
		maxDate: window.resArr[0], // указываем максимально возможную дату в календаре, на кот. есть курс валют в списке
		minDate: window.resArr[window.resArr.length], // указываем минимально возможную дату в календаре, на кот. есть курс валют в списке
		enable: window.resArr, // доступные даты

		onChange: (selectedDates, dateStr, instance) => { // функция изменения даты в календаре
			const newDate = dateStr; // если дата была изменена в календаре, то записываем новую дату в поле даты

			var xhr = new XMLHttpRequest();
			xhr.open('GET', componentPath + "/ajax.php?date=" + dateStr, false); // ?date=2024-02-16
			xhr.send();

			if (xhr.status != 200) {
				if (xhr.status == 404) {
					// обработка 404 ошибки
					document.getElementById("ajax_error").innerHTML = ( 'Ошибка ' + xhr.status + ': файл запроса не существует' );
				} else {
					document.getElementById("ajax_error").innerHTML = ( 'Ошибка ' + xhr.status + ': что-то пошло не так' );
				} 
			} else {
				// вывести результат если статус 200 ОК
				const resultText = xhr.responseText; // responseText -- текст ответа
				document.getElementById('hidden').innerHTML = resultText;

				const pureResult = resultText.replace(/[^a-z0-9.-_]/gi, '');

				if (pureResult  == 'ERR_DATE_FORMAT') {
					document.getElementById("ajax_error").innerHTML = 'Неверный формат даты. Выберите дату в календаре или укажите дату в формате ГГГГ-ММ-ДД.';
				} else if (pureResult  == 'ERR_DATE_EMPTY') {
					document.getElementById("ajax_error").innerHTML = 'Дата не выбрана: выберите дату.';
				} else {
					const newUsdExchange = document.getElementById('USD_' + dateStr).textContent;
					const newEurExchange = document.getElementById('EUR_' + dateStr).textContent;
				
					inputUsd.value = newUsdExchange; // подменяем текущий курс USD на новый
					inputEur.value = newEurExchange; // подменяем текущий курс EUR на новый
				}

			}
		}
	});
</script>