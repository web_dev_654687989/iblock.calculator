<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

$arComponentDescription = array(
	"NAME" => GetMessage("WZ_NAME"),
	"DESCRIPTION" => GetMessage("WZ_DESC"),
	"SORT" => 200,
	"CACHE_PATH" => "Y",
	"PATH" => array(
		"ID" => "calculator",
		"CHILD" => array(
			"ID" => "support",
			"NAME" => GetMessage("WZ_NAME")
		),
	),
);

?>
