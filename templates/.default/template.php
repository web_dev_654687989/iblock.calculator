<?php if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?php CJSCore::Init(array('ajax')); // подключаем ajax ?>

<?php 
	//подключаем js-библиотеку html2canvas
	$this->addExternalJS("https://html2canvas.hertzen.com/dist/html2canvas.js");

	// подключаем библиотеку flatpickr и её стили
	$this->addExternalJS("https://npmcdn.com/flatpickr/dist/flatpickr.min.js");
	$this->addExternalCss("https://cdn.jsdelivr.net/npm/flatpickr/dist/flatpickr.min.css");
	$this->addExternalJS("https://npmcdn.com/flatpickr/dist/l10n/ru.js");
?>

<?php
$res = CIBlock::GetList(
	Array(),
	Array(
		"TYPE"=>$arParams["IBLOCK_TYPE"],
		"ID"=>$arParams["IBLOCK_ID"]
	),
	false
);

while($arRes = $res->Fetch())
{
	$iblockCode = $arRes['CODE'];
}

// определяем $template в зависимости от того, какой выбран инфоблок
if(strpos($iblockCode, 'mkas') !== false){
    $template = 'mkas'; // если mkas
} elseif (strpos($iblockCode, 'mac') !== false) {
   $template = 'mac';   // если mac
} else {
    $template = false;    // если ни то, ни другое
}
?>

<?php
$path = $this->GetFolder(); // получаем путь до папки шаблона компонента

if($template == 'mkas'|| $template == 'mac'){
    // если $template содержит mkas или mac, подключаем соответствующий шаблон
	echo '<script src="' .$path. '/js/new' .$template. '_script.js"></script>'; // подключаем нужный js-скрипт
	include_once 'new'.$template.'_template.php'; // подключаем нужный template.php
} else {
    // если ни одно из двух
    echo 'Шаблон компонента не найден<br>';
}
?>