<?php require_once($_SERVER['DOCUMENT_ROOT']. "/bitrix/modules/main/include/prolog_before.php"); ?>
<?php if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>

<?php
// подключаем модуль currency
\Bitrix\Main\Loader::includeModule('currency');?>

<?php
$today = date("d.m.Y"); // сегодняшняя дата в формате d.m.Y
$dateFrom = "01.07.1992"; // min
$dateTo = date("d.m.Y", strtotime("+1 days", strtotime($today))); // max

// Получим все доступные даты из списка курсов валют
$arFilter = array(
	"CURRENCY" => "USD",
	"DATE_RATE" => $dateFrom, // формат даты '16.12.2014',
	"!DATE_RATE" => $dateTo
);
$by = "date";
$order = "desc";
$db_rate = CCurrencyRates::GetList($by, $order, $arFilter);

$availableDates = array();  // массив для записи дат и курсов валюты USD

while($ar_rate = $db_rate->Fetch())
{
	$availableDates[] = $ar_rate["DATE_RATE"]; // пишем доступные даты в массив
}

foreach($availableDates as $value){
	echo $value . ', ';
}
