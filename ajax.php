<?php require_once($_SERVER['DOCUMENT_ROOT']. "/bitrix/modules/main/include/prolog_before.php"); ?>
<?php if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>

<?php
// подключаем модуль currency
\Bitrix\Main\Loader::includeModule('currency');
?>

<?php
// GET-запрос https://domain.ru/bitrix/components/local/iblock.calculator/ajax.php?date=2024-02-16

// проверка данных
if(isset($_GET['date']) && !empty($_GET['date']) && is_string($_GET['date'])){ // существует ли переменная, она не равна NULL и является строкой
    if (preg_match("#^[0-9\.]+$#", $_GET['date'])) {
        $date = date("d.m.Y", strtotime($_GET['date']));  // установленная пользователем дата в формате d.m.Y
        $dateFrom = date("d.m.Y", strtotime("-42 days", strtotime($date))); // min
        $dateTo = date("d.m.Y", strtotime("+1 days", strtotime($date))); // max

		// Выведем все курсы USD, отсортированные по дате

		$arFilter = array(
			"CURRENCY" => "USD",
			"DATE_RATE" => $dateFrom, // формат даты '16.12.2014',
			"!DATE_RATE" => $dateTo
		);
		$by = "date";
		$order = "desc";
		$dbRate = CCurrencyRates::GetList($by, $order, $arFilter);

		$usdExchangeRates = array();  // массив для записи дат и курсов валюты USD

		while($arRate = $dbRate->Fetch())
		{
			$usdExchangeRates[$arRate["DATE_RATE"]] = $arRate["RATE"];
		}

		foreach($usdExchangeRates as $usdExchangeDate => $usdExchangeRate){
			if (array_key_exists($date, $usdExchangeRates)) {
				$firstKey = array_key_first($usdExchangeRates);
				$usdExchangeRate = $usdExchangeRates[$firstKey];  // ключ найден - выводим курс USD на дату из списка
			} else {
				$firstKey = array_key_first($usdExchangeRates);
				$usdExchangeRate = $usdExchangeRates[$firstKey];  // ключ НЕ найден - выводим последний курс валюты из списка
			}
		}

		// Выведем все курсы EUR, отсортированные по дате

		$arFilter = array(
			"CURRENCY" => "EUR",
			"DATE_RATE" => $dateFrom, // формат даты '16.12.2014',
			"!DATE_RATE" => $dateTo
		);
		$by = "date";
		$order = "desc";
		$dbRate = CCurrencyRates::GetList($by, $order, $arFilter);

		$eurExchangeRates = array();  // массив для записи дат и курсов валюты USD

		while($arRate = $dbRate->Fetch())
		{
			$eurExchangeRates[$arRate["DATE_RATE"]] = $arRate["RATE"];
		}

		foreach($eurExchangeRates as $eurExchangeDate => $eurExchangeRate){
			if (array_key_exists($date, $eurExchangeRates)) {
				$eurExchangeRate = $eurExchangeRates[$date]; // ключ найден - выводим курс EUR на дату из списка
			} else {
				$firstKey = array_key_first($eurExchangeRates);
				$eurExchangeRate = $eurExchangeRates[$firstKey];  // ключ НЕ найден - выводим последний курс EUR из списка
			}
		}

		if ((isset($usdExchangeRate) || isset($eurExchangeRate)) && (!empty($usdExchangeRate) || !empty($eurExchangeRate))) {
			echo '<span id="USD_' . $date . '">' . $usdExchangeRate . '</span>' . '<span id="EUR_' . $date . '">' . $eurExchangeRate . '</span>';
		 } elseif (!isset($usdExchangeRate) || !isset($eurExchangeRate)) {
			echo '<span id=ERR_DATE_FORMAT>ERR_DATE_FORMAT</span>';
		 } else {
			echo '<span id="ERR_DATE_EMPTY">ERR_DATE_EMPTY</span>';
		 } 


    } else {
		echo 'ERR_DATE_FORMAT'; // ошибка, если формат даты неверный
    }
} else {
	echo 'ERR_DATE_EMPTY'; // ошибка, если дата пустая
}
